import React, { Component, PropTypes } from 'react';

const propTypes = {
  onMount: PropTypes.func,
  buttonValue: PropTypes.string
};

class CommentList extends Component {
  constructor(props) {
    super(props);

    this.displayName = 'myList';
  }

  componentDidMount() {}

  render() {
    return (
      <div>
        <span>Hello</span>
        <button onClick={this.props.onButtonClick}>{this.props.buttonValue}</button>
      </div>
    )
  }
}

CommentList.propTypes = propTypes;
export default CommentList;
