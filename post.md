# Setting up a TDD environment with React, Enzyme and Mocha

With the current state of front-end web development, it's way too easy to get lost in a sea of options when it comes to choosing a stack to develop with. The same goes for setting up a test environment; depending on which frameworks you're using, you might be inclined to use different libraries and test runners to better suit the application workflow and logic.

In this post, we're going to quickly cover how to set up a working test environment (with a TDD watcher) for React, using Airbnb's Enzyme library and Mocha/Chai as our test and assertion libraries. Also included is a sample Webpack configuration to actually be able to run the development environment, but it will be beyond the scope of this introduction. Check out the full repo on Github.

## Getting Started

Let's start a project from scratch by creating a clean `npm` package by running `npm init -y`.

The structure for this project will be the usual application structure: our code will live in the `src` folder and our tests will live under the `test` folder. Setting up our structure, then:

`mkdir src dist test`

And our files:

`touch src/main.js webpack.config.js index.html`

## Dependencies and project structure

First off, we know we are going to make use of ES2015 JavaScript syntax. For this to work, we need to install Babel and its dependencies so we can transpile the code, both for the source and our tests. So let's grab our dependencies, also installing React in the process:

`npm i babel-register babel-loader babel-core babel-preset-react babel-preset-es2015 react react-dom --save-dev`

That was a lot of dependencies! All we're doing is grabbing all the presets so we can teach Babel to expect React and JSX syntax, as well as ES2015 code. We'll wire things up by creating a `.babelrc` file in the root:

`touch .babelrc`

In this file, copy and paste the following configuration for our project:

```
{
  "presets": ["react", "es2015"]
}
```

This ensures Babel knows how to handle JSX and the ES2015 syntax we're writing. Note that we could also have done this in the Webpack configuration file, but I personally prefer to keep this kind of configuration separate from the actual build scripts.

## Testing Dependencies

Let's grab our testing libraries first. We'll be using **Mocha** as our test runner, **Chai** as our expectation library, **Sinon** for creating stubs and mocks whenever necessary, and **Enzyme** as a helper library for testing React Components. We'll go over Enzyme a bit later on, after we set things up.

`npm i mocha chai sinon enzyme react-addons-test-utils --save-dev`

## Setting up the Testing framework

Once we have tests and everything wired up, we could manually run the scripts to run Mocha with all the necessary parameters. Luckily, we can make use of npm scripts to achieve this, so we don't have to remember all the command line arguments. Ideally, we want to simply run `npm test` and have all the magic happen for us.

Open up your `package.json` file and look for the `scripts` object. Copy the following:

```
"scripts": {
    "test": "mocha --compilers js:babel-register --require ./test/helpers.js --require ./test/dom.js --recursive",
    "tdd": "npm test -- --watch",
    "dev": "webpack-dev-server --port 3000 --devtool eval --progress --colors --hot --content-base dist",
    "build": "webpack -p"
  },
```

And while we're at it, let's also setup a simple Webpack configuration file. In your `webpack.config.js`, dump the following:

```
var webpack = require('webpack');
var path = require('path');

var config = {
  entry: [
    'webpack/hot/dev-server',
    'webpack-dev-server/client?http://localhost:3000',
    './src/main.js'
  ],
  externals: {
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true
  },
  resolve: {
    root: [
      path.resolve(__dirname, './src')
    ],
    extensions: ['', '.js', '.json', '.jsx']
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'                  
  },
  module: {
    loaders: [
      { test: /\.js$/, loaders: ['babel'], exclude: /node_modules/ }
    ]
  }
}

module.exports = config;
```

So we now have a development server running in the `3000` port. This isn't necessary for our tests, though, but having this around makes up for a nice simple starter boilerplate to have at hand!

**Before moving on, let's have a closer look at our test npm test task:**

`"test": "mocha --compilers js:babel-register --require ./test/helpers.js --require ./test/dom.js --recursive"`

You may notice that we're requiring two files we still haven't created. `helpers.js` will be a helper file with some globals we'll be using for tests (so we don't have to import them all the time), and `dom.js` will create a fake DOM environment for them. The `--recursive` flag will make Mocha looks for all files and directories under the test folder.

## Helper files

So let's create the `test/helpers.js` file, and include the following:

```
import { expect } from 'chai';
import sinon from 'sinon';

import { expect } from 'chai';
import { sinon, spy } from 'sinon';
import { mount, render, shallow } from 'enzyme';

global.expect = expect;
global.sinon = sinon;
global.spy = spy;

global.mount = mount;
global.render = render;
global.shallow = shallow;
```

And `test/dom.js` will have our mocked DOM:

```
/* setup.js */
var jsdom = require('jsdom').jsdom;
var exposedProperties = ['window', 'navigator', 'document'];

global.document = jsdom('');
global.window = document.defaultView;
Object.keys(document.defaultView).forEach((property) => {
  if (typeof global[property] === 'undefined') {
    exposedProperties.push(property);
    global[property] = document.defaultView[property];
  }
});

global.navigator = {
  userAgent: 'node.js'
};
```

## Writing our first test

In proper TDD fashion, let's write the first test for our initial, yet unwritten React component! We'll write a simple component which will be a comment item, containing an `<li>` tag with custom text.

Let's create the test file: `touch ./test/Comment.spec.js`

The tests are written in the traditional "describe, it should, expect" fashion. If we were to write the simplest of a passing test, we could do the following:

```
describe('a passing test', () => {
  it('should pass', () => {
    expect(true).to.be.true;
  });
});
```

And indeed, `true` is `true`, so that passes. So let's focus on our `Comment.spec.js` file. We want our `Comment` component to be of type `<li>` and to receive a custom comment, also having a class name of `comment-item`.

```
import React from 'react';
import Comment from '../src/components/Comment';

describe('Comment item', () => {
  const wrapper = shallow(<Comment />);

  it('should be a list item', () => {
    expect(wrapper.type()).to.eql('li');
  });
});
```

If we run `npm test` now, our test will fail because we still haven't written our component. So let's open up `src/components/Comment.js` and create it!

### Creating a Component

```
import React, { Component } from 'react';

const styles = {
  height: '100%',
  background: '#333'
}

class Comment extends Component {
  render() {
    return (
      <li>
        <span className='comment'>{this.props.comment}</span>
      </li>
    )
  }
}

export default Comment;
```
